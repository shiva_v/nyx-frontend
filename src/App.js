import React, { Component } from 'react';

import Auth from './functional/auth';
import AppShell from './functional/app/appShell';

import './App.css';

class App extends Component {
  state = {
    authenticated: false,
    token: ''
  }
  
  constructor() {
    super();
    this.changeAuthState = this.changeAuthState.bind(this);
  }
  
  componentDidMount() {
    const token = localStorage.getItem('authToken');
    console.log(token + ' Token')
    if(token && this.checkValidity(token)) {
      this.setState({
        token: token,
        authenticated: true
      }, () => console.log('authenticated: ' + this.state.authenticated));
    }
  }
  
  checkValidity(token) {
    return true;
    // Place a validity check to check if token is valid or expired and clear the token from localStorage if token has expired
  }
  
  changeAuthState(authState, token) {
    this.setState({
      authenticated: authState,
      token
    })
  }

  renderAuthScreens() {
    return(
      <div className="app-body">
        <Auth authHandler={this.changeAuthState}/>
      </div>
    )
  }

  renderApp() {
    return(
      <AppShell />
    )
  }

  render() {
    const { authenticated } = this.state;
    return (
      <div className="App">
        {authenticated ? this.renderApp() : this.renderAuthScreens()}
      </div>
    );
  }
}

export default App;
