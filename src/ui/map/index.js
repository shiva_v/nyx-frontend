import React, {Component} from 'react';

class Map extends Component {

  state = {
    viewport: {
      width: 400,
      height: 400,
      latitude: 12.94994,
      longitude: 77.555,
      zoom: 16
    }
  };

  render() {
    return (
      <div>Renders a map</div>
    );
  }
}

export default Map;
