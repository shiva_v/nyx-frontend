export default class Utils {
  static isMobile() {
    var browserUtil = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (browserUtil.Android() || browserUtil.BlackBerry() || browserUtil.iOS() || browserUtil.Opera() || browserUtil.Windows());
        }
    };
    return browserUtil;
  }
}
