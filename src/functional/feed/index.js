import React, { Component } from 'react';

import Map from '../../ui/map';

class Feed extends Component {
  render() {
    return(
      <div className="feed">
        <h3>
          Feed
        </h3>
        <Map />
      </div>
    )
  }
}

export default Feed;
