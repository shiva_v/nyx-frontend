import React, { Component } from 'react';
import axios from 'axios';

import Input from '../../ui/input';
import Button from '../../ui/button';

import { config } from '../../config';

class Login extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      email: '',
      password: '',
      errorText: ''
    }
  }

  componentDidMount() {

  }

  renderErrorText() {
    return(
      <div className="error-container">
        <span className="error-text">
          {this.state.errorText}
        </span>
      </div>
    )
  }

  handleChange(e, data) {
    this.setState({
      [e.target.name] : e.target.value
    });
  }

  handleSubmit() {
    let loginParams = {
      email: this.state.email,
      password: this.state.password
    }
    console.log(config.baseURL);
    axios.post(config.baseURL + 'auth/login/', loginParams)
      .then((response) => {
          console.log('The auth token is : ' + response.data.token);
          localStorage.setItem('authToken', response.data.token);
          this.props.authHandler(true, response.data.token);
      })
      .catch((err) => {
        if((err.response.status===404 && err.response.data==="No user found.") || err.response.status===401) {
          console.log('Error logged')
          this.setState({
            errorText: 'Wrong email/password'
          })
          console.log(err.response);
        }
      })
  }

  render() {
    return(
      <div className="login-form">
        <Input
          name={"email"}
          type={"text"}
          placeholder={"Your email"}
          changeHandler={this.handleChange}
          focusHandler={() => {this.setState({errorText: ''})}}
        />
        <Input
          name={"password"}
          type={"password"}
          placeholder={"Your password"}
          changeHandler={this.handleChange}
          focusHandler={() => {this.setState({errorText: ''})}}
        />
        {this.renderErrorText()}
        <Button
          buttonText={"Submit"}
          clickHandler={this.handleSubmit}
          variant={'info'}
        />
      </div>
    )
  }
}

export default Login;
