import React, { Component } from 'react';
import {
  Route
} from 'react-router-dom';

import Heading from '../../ui/header';

import Home from '../home';
import Feed from '../feed';
import Social from '../social';
import Navbar from '../navbar';
import Footer from '../footer';

class AppShell extends Component {
  render() {
    return(
      <div className="app-body">
        <div className="app-container container">
          <Heading
            headerText={'nyx'}
            classes={['brand-header']}
          />
          <Navbar />
          <div>
            <Route path="/" exact component={Home}/>
            <Route path="/feed" component={Feed}/>
            <Route path="/social" component={Social}/>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

export default AppShell;
