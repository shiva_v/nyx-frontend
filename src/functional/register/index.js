import React, { Component } from 'react';
import axios from 'axios';

import Input from '../../ui/input';
import Button from '../../ui/button';

import { config } from '../../config';

class Register extends Component {

  state = {
    errorText: ''
  }

  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {

  }

  renderErrorText() {

  }

  handleSubmit() {
    let registerParams = {
      email: this.state.email,
      password: this.state.password,
      phone: this.state.phone
    }
    console.log(config.baseURL);
    axios.post(config.baseURL + 'auth/register/', registerParams)
      .then((response) => {
          console.log('The auth token is : ' + response.data.token);
          localStorage.setItem('authToken', response.data.token);
          this.props.authHandler(true, response.data.token);
      })
      .catch((err) => {
        if(err.response.status===404 && err.response.data==="No user found.") {
          console.log('Error logged')
          this.setState({
            errorText: 'Wrong email/password'
          })
          console.log(err.response);
        }
      })
  }

  renderPhone() {
    return(
      <div className="phone-row">
        <div className="country-code">
          <div className="code">
            +91
          </div>
        </div>
        <Input
          class={'phone-no'}
          name={"phone"}
          type={"number"}
          placeholder={"Phone"}
          changeHandler={this.handleChange}
          focusHandler={() => {this.setState({errorText: ''})}}
        />
      </div>
    )
  }

  handleChange(e) {
    this.setState({
      [e.target.name] : e.target.value
    });
  }

  render() {
    return(
      <div className="register-form">
        <Input
          name={"email"}
          type={"text"}
          placeholder={"Desired username"}
          changeHandler={this.handleChange}
          focusHandler={() => {this.setState({errorText: ''})}}
        />
        <Input
          name={"password"}
          type={"password"}
          placeholder={"Set a password"}
          changeHandler={this.handleChange}
          focusHandler={() => {this.setState({errorText: ''})}}
        />
        {this.renderPhone()}
        {this.renderErrorText()}
        <Button
          buttonText={"Submit"}
          clickHandler={this.handleSubmit}
          variant={'info'}
        />
      </div>
    )
  }
}

export default Register;
