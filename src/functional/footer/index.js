import React, { Component } from 'react';
import '../../footer.css';

class Footer extends Component {
  render() {
    return(
      <footer className="footer">
        <div className="container">
            <h5 className="subtitle is-5">
              Built with <i className="fa fa-heart" aria-hidden="true" style={{color: 'red'}}></i> by <strong>Shiva Venkatesh</strong>
            </h5>
        </div>
      </footer>
    )
  }
}

export default Footer;
