import React, { Component } from 'react';

import '../../place.css';

export default class Place extends Component {
  constructor(props) {
    super(props)
    this.state = {
      color: '#' + this.props.selectedPlace.restaurant.user_rating.rating_color
    }
  }

  componentDidMount() {

  }

  render() {
    return(
      <div className="place-details">
        <div className="place-header">
          <div className="back-button" onClick={this.props.backButtonHandler}>
            <i className="fa fa-chevron-left" aria-hidden="true"></i>
          </div>
          <div className="subtitle is-3">
            {this.props.selectedPlace.restaurant.name}
          </div>
        </div>

        <div className="place-images">

        </div>

        <div className="place-address subtitle is-5">
          {this.props.selectedPlace.restaurant.location.address}
        </div>
        <div className="place-rating subtitle is-3" style={{backgroundColor: this.state.color, opacity: '0.8'}}>
          <div className="rating-number">
            <div className="rating">
              {this.props.selectedPlace.restaurant.user_rating.aggregate_rating}
              {'/5'}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
