import React, { Component } from 'react';
import axios from 'axios';

import Card from '../../ui/card';
import Spinner from '../../ui/spinner';
import { config } from '../../config';

import Place from './place'
import '../../home.css';

class Home extends Component {
  state = {
    places: [],
    loading: true,
    selectedPlace: {}
  }

  constructor() {
    super();
    this.backButtonHandler = this.backButtonHandler.bind(this);
  }

  backButtonHandler() {
    this.setState({
      selectedPlace: {}
    });
  }

  componentDidMount() {
    var that = this;
    navigator.geolocation.getCurrentPosition(function(location) {
      axios.get(config.baseURL + 'places', {
        params: {
          lat: location.coords.latitude,
          long: location.coords.longitude,
          radius: 5000
        }
      }).then((response) => {
          var places = JSON.parse(JSON.stringify(response.data))
          that.setState({
            places: places,
            loading: false
          })
        })
        .catch((err) => {
          console.log(err)
          // Show error page
        })
    });
  }

  setPlace(place) {
    this.setState({
      selectedPlace: place
    })
  }

  renderPlaceDetails() {
    return(
      <Place selectedPlace={this.state.selectedPlace} backButtonHandler={() => this.backButtonHandler()}/>
    )
  }

  renderCards() {
    if(this.state.loading) {
      return(
        <Spinner />
      )
    } else {
      return(
        this.state.places.map((place) => {
          return(
            <Card key={place.restaurant.id} onClick={() => this.setPlace(place)}>
              <div className="card-body">
                <nav className="level">
                  <div className="level-left left-box">
                    <div className="heading-name">
                      <h3 className="subtitle is-3">
                        {place.restaurant.name}
                      </h3>
                    </div>
                    <div className="locality-subHeading">
                      <h5 className="subtitle is-5">
                        {place.restaurant.location.locality_verbose}
                      </h5>
                    </div>
                  </div>
                  <div className="level-right thumbnail-container">
                    <img src={place.restaurant.thumb} alt={place.restaurant.name} />
                  </div>
                </nav>
              </div>
            </Card>
          )
        })
      )
    }
  }

  render() {
    return(
      <div className="home">
        {Object.keys(this.state.selectedPlace).length === 0 && this.state.selectedPlace.constructor === Object ? this.renderCards() : this.renderPlaceDetails()}
      </div>
    )
  }
}

export default Home;
