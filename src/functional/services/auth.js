const AuthService = {
  isAuthenticated: false,
  authenticate(cb) {
    if(localStorage.getItem('nyxToken')) {
      this.isAuthenticated = true
      setTimeout(cb, 100)
    }
  },
  logout(cb) {
    localStorage.removeItem('nyxToken');
    this.isAuthenticated = false
    setTimeout(cb, 100)
  }
}

export default AuthService;
